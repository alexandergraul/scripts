#!/bin/sh

# This script creates a session of tmux with a preconfigured set of windows and
# panes.

# Create a new session with the name 'dev'
tmux new-session -s 'dev' -n 'HTOP' -d htop

# Set up 'CODE' window
if [ $# -gt 0 ]; then
  CODEDIR=$1
else
  CODEDIR='/home/alex/programming'
fi
tmux new-window -c $CODEDIR -n 'CODE'
tmux send-keys 'l' C-m
tmux split-window -h -b -p 70 -c $CODEDIR
tmux send-keys 'nvim' C-m

# Set up 'SSH' window
tmux new-window -n 'SSH'

# Set up chat
tmux new-window -n 'CHAT'
tmux send-keys 'weechat' C-m

# Set up mail
tmux new-window -n 'MAIL'
tmux send-keys 'neomutt' C-m

tmux select-window -t 'CODE'
tmux attach -t 'dev'
