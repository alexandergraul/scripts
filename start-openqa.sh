#!/bin/bash

# Helper script to start my openQA laptop setup

if [[ $UID != 0 ]]; then
    echo "Must be run as root!";
    exit 1;
fi

echo "Mounting test cases...";
# mount openSUSE test cases
mount --bind $HOME/qa/openqa_share_tests/os-autoinst-distri-opensuse /var/lib/openqa/share/tests/opensuse;

# mount SLE test cases
mount --bind $HOME/qa/openqa_share_tests/os-autoinst-distri-opensuse /var/lib/openqa/share/tests/sle;

echo "Starting openQA services";
# start systemd services
systemctl start openqa-webui.service
systemctl start openqa-scheduler.service
systemctl start openqa-worker@1.service

echo "Testing connection to openQA";
return_code=$(curl --head --write-out '%{http_code}' --silent --output /dev/null localhost)

if [[ $return_code != 200 ]]; then
    echo "Successful.";
else
    echo "Connection failed, manual investigation required.";
    exit 2;
fi

exit 0;
