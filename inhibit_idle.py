#!/usr/bin/python3
"""
Wrapper around `systemd-inhibit` to toggle idle state.

Documentation on systemd-logind's Inhibitor Lock Developer Documentation can be
found at:

    https://www.freedesktop.org/wiki/Software/systemd/inhibit
"""
import subprocess

INHIBITOR_NAME = 'inhibit_idle.py'


def start_inhibitor():
    '''Create inhibitor lock for 24h.'''
    one_day = '86400'
    subprocess.run(
        ['systemd-inhibit',
         f'--who={INHIBITOR_NAME}',
         '--why=User is busy.',
         'sleep',
         one_day])


def kill_process(pid: str) -> bool:
    '''Kill process with given PID'''
    killer = subprocess.run(['kill', pid])
    return killer.returncode == 0


def inhibitor_pid() -> str:
    '''Get inhibitor PID from `systemd-inhibit --list`.'''
    inhibitor_list = subprocess.run(['systemd-inhibit', '--list'],
                                    capture_output=True,
                                    text=True)
    # structure of splitted inhibitor:
    # ['WHO', 'UID', 'USER', 'PID', 'COMM', 'WHAT', 'WHY', 'MODE']
    inhibitors = [x.split() for x in inhibitor_list.stdout.split('\n') if x]

    pid = ''
    for inhibitor in inhibitors:
        if inhibitor[0] == INHIBITOR_NAME:
            pid = inhibitor[3]
            break

    return pid

if __name__ == '__main__':
    PID = inhibitor_pid()
    if PID:
        kill_process(PID)
        subprocess.run(['notify-send', 'Idle inhibitor disabled'])
    else:
        subprocess.run(['notify-send', 'Idle inhibitor enabled'])
        start_inhibitor()
