#!/bin/sh

NAME="discord"
VERSION=0.0.5
URL="https://dl.discordapp.net/apps/linux/$VERSION/$NAME-$VERSION.tar.gz"
LOCALFILE=$NAME-$VERSION.tar.gz

if [ -e $LOCALFILE ]; then
    echo "$LOCALFILE already downloaded"
else
    echo "Downloading $NAME version $VERSION now..."
    curl -o $LOCALFILE $URL
fi

if [ ! -e $LOCALFILE ]; then
    echo "Download failed, exiting..."
    exit 1
fi
echo "Extracting from archive..."
tar -xzf $LOCALFILE
echo "Deleting archive and moving files..."
rm $LOCALFILE

mv ./Discord /home/alex/bin/
cd /home/alex/bin/Discord
echo "Setting up desktop file..."
sed 's!/usr/share/discord/Discord!/home/alex/bin/Discord/Discord!' discord.desktop > discord2.desktop
sed 's!on=discord!on=/home/alex/bin/Discord/discord.png!' discord2.desktop > discord.desktop
rm discord2.desktop
cp discord.desktop /home/alex/.local/share/applications/

if [ ! -e /usr/lib64/libc++.so.1 ]; then
    echo "libc++1 not found, installing now"
    sudo zypper --non-interactive install libc++1
else
    echo "libc++1 was found"
fi
echo "Done!"
