#!/bin/bash

if [ ! -e "$(which setxkbmap)" ]; then
    echo "No setxkbmap found, exiting."
    exit 1
fi

setxkbmap eu
echo "EurKEY was enabled"
echo "Also setting CAPS as CONTROL..."
setxkbmap -option "ctrl:nocaps"
echo "Done"
exit 0
